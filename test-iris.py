import numpy as np
from random import shuffle
from som import SOM
from utils import normalize_dimensions


def load():
    data = []
    classes = list()
    with open('datasets/iris.data.txt') as fp:
        for line in fp:
            l = line.strip().split(',')
            if len(l) != 5: continue
            v = np.array([float(e) for e in l[:-1]])
            if l[-1] not in classes:
                classes.append(l[-1])
            data.append([classes.index(l[-1]), v])
    normalize_dimensions(data)
    return data


if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    TRAIN_LEN = 100
    SZ = 10

    som = SOM(SZ,SZ,K)
    som.train(data[:TRAIN_LEN], 200, alpha0=0.95, radius0=SZ*0.5, k=0.3)
    som.train(data[:TRAIN_LEN], 500, alpha0=0.05, radius0=1.5, k=0.15)
    som.tag_classes(data[:TRAIN_LEN], clss[:TRAIN_LEN])

    l = []
    n, k, u = 0, 0, 0
    for cls, vec in zip(clss[TRAIN_LEN:], data[TRAIN_LEN:]):
        guess = som.predict(vec)
        if guess != None:
            if guess[0] == cls:
                k += 1
            n += 1
            l.append(int(guess[0]) - int(cls))
        else:
            u += 1

    print('{} samples testeds'.format(n + u))    
    print('correct guesses: {:%}, {} unkowns'.format(k / n, u))

    a = np.array(l)
    print('error average = {}, std = {}'.format(np.fabs(a).mean(), a.std()))

    som.visualize()
