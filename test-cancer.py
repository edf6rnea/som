import numpy as np
from random import shuffle
from som import SOM
from utils import normalize_dimensions


# deal with missing values
def fn(e):
    try:
        return int(e)
    except:
        return 0

def load():
    data = []
    with open('datasets/breast-cancer-wisconsin.data.txt', 'r') as fp:
        for line in fp:
            l = line.strip().split(',')
            if len(l) != 11: continue
            v = np.array([fn(e) for e in l[1:-1]])
            data.append([l[-1], v])
    #normalize_dimensions(data) # pourquoi ça a un tel impact ????
    return data

if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    TRAIN_LEN = 679
    SZ = 10

    som = SOM(SZ,SZ,K)
    som.train(data[:TRAIN_LEN], 200, alpha0=0.95, radius0=SZ*0.5)
    som.train(data[:TRAIN_LEN], 500, alpha0=0.05, radius0=1.5)
    som.tag_classes(data[:TRAIN_LEN], clss[:TRAIN_LEN])

##    l = []
##    n, k, u = 0, 0, 0
##    for cls, vec in zip(clss[TRAIN_LEN:], data[TRAIN_LEN:]):
##        guess = som.predict(vec)
##        if guess != None:
##            if guess[0] == cls:
##                k += 1
##            n += 1
##            l.append(int(guess[0]) - int(cls))
##        else:
##            u += 1
##
##    print('{} samples testeds'.format(n + u))    
##    print('correct guesses: {:%}, {} unkowns'.format(k / n, u))
##
##    a = np.array(l)
##    print('error average = {}, std = {}'.format(np.fabs(a).mean(), a.std()))

    som.visualize()
