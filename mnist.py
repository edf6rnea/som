#adapted from : http://g.sweyla.com/blog/2012/mnist-numpy/

import os, struct
import gzip
from array import array as pyarray
from numpy import array, int8, uint8, zeros
from scipy.ndimage import zoom as fzoom

def load_mnist(dataset="training", digits=range(10), path="datasets", zoom=None, crop=None):
    """
    Loads MNIST files into 3D numpy arrays

    Adapted from: http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
    """

    if dataset == "training":
        fname_img = os.path.join(path, 'train-images-idx3-ubyte.gz')
        fname_lbl = os.path.join(path, 'train-labels-idx1-ubyte.gz')
    elif dataset == "testing":
        fname_img = os.path.join(path, 't10k-images-idx3-ubyte.gz')
        fname_lbl = os.path.join(path, 't10k-labels-idx1-ubyte.gz')
    else:
        raise ValueError("dataset must be 'testing' or 'training'")

    flbl = gzip.open(fname_lbl, 'rb')
    magic_nr, size = struct.unpack(">II", flbl.read(8))
    lbl = pyarray("b", flbl.read())
    flbl.close()

    fimg = gzip.open(fname_img, 'rb')
    magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
    img = pyarray("B", fimg.read())
    fimg.close()

    ind = [ k for k in range(size) if lbl[k] in digits ]
    N = len(ind)

    images = []
    labels = []
    for i in range(len(ind)):
        arr = array(img[ ind[i]*rows*cols : (ind[i]+1)*rows*cols ]).reshape((rows, cols))
        if crop is not None:
            arr = arr[crop:-crop,crop:-crop]
        if zoom is not None:
            arr = fzoom(arr, zoom)
        images.append(arr.flatten())
        labels.append(lbl[ind[i]])

    return array(images, dtype=uint8), array(labels, dtype=uint8)
