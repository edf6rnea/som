import numpy as np
from random import shuffle
from som import SOM
from utils import normalize_dimensions


def load():
    data = []
    with open('datasets/10clusters.vec', 'r') as fpv, open('datasets/10clusters.cls', 'r') as fpc:
        for lv, lc in zip(fpv, fpc):
            l = [float(x) for x in lv.split()[:-1]]
            v = np.array(l)
            c = lc.split()[-1]
            data.append([c, v])
    normalize_dimensions(data)
    return data


if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    SZ = 20

    som = SOM(SZ,SZ,K)
    som.train(data, 1000, alpha0=0.95, radius0=SZ*0.5)
    som.train(data, 5000, alpha0=0.05, radius0=1.5)
    som.tag_classes(data, clss)

    som.visualize()
