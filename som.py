#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from collections import defaultdict, Counter
from operator import itemgetter
from itertools import product
from random import choice, shuffle
import numpy as np
from time import time


def randomize(it):
    l = list(it)
    while True:
        shuffle(l)
        for e in l:
            yield e


class SOM:
    def __init__(self, h, w, dim, topology='rect', verbose=True):
        self.h = h
        self.w = w
        self.dim = dim
        # randomly initialize network
        self.net = np.random.rand(h, w, dim).astype(np.float32)
        self.verbose = verbose
        assert topology in ['rect', 'hexa'], 'invalid topology parameter'
        self.topology = topology

    def bmu(self, v):
        """
        find the best match unit
        (use euclidean distance)
        """
        distances = ((self.net - v) ** 2).sum(axis=2) # ** 0.5
        #return np.unravel_index(distances.argmin(), (self.w, self.h))
        return choice(list(zip(*np.where(distances == distances.min()))))

    def train(self, data, tmax, *,
                alpha0=0.5, alphaN=0.001, acooling='linear', 
                radius0=None, radiusN=None, rcooling='linear', k=0.25):
        """
        train network accordingly to the input data
        and given parameters
        """

        # setup parameters
        if radius0 is None:
            radius0 = min(self.h, self.w) * 0.5
        if radiusN is None:
            radiusN = 0.
        assert alpha0 > alphaN
        assert radius0 > radiusN
        if rcooling == 'linear':
            fradius = lambda t: radiusN + (radius0 - radiusN) * (1 - t / tmax)
        elif rcooling == 'exponential':
            fradius = lambda t: radius0 * (radiusN / radius0) ** (t / tmax)
        else:
            raise RuntimeError('bad radius cooling %s' % rcooling)
        if acooling == 'linear':
            falpha = lambda t: alphaN + (alpha0 - alphaN) * (1 - t / tmax)
        elif acooling == 'exponential':
            falpha = lambda t: alpha0 * (alphaN / alpha0) ** (t / tmax)
        else:
            raise RuntimeError('bad alpha cooling %s' % acooling)

        if self.verbose:
            t1 = time()
            print('  training SOM of {}x{} units on {} dimensions'.format(self.w, self.h, self.dim))
            print('\tnb samples = {}, nb iterations = {}'.format(len(data), tmax))
            print('\ta0 = {:.3}, cooling = {}, r0 = {:.3}, cooling = {}'.format(alpha0, acooling, radius0, rcooling))

        # prepare distances matrix
        r0 = int(radius0) + 1
        X, Y = np.mgrid[-r0:r0+1., -r0:r0+1.]
        if self.topology == 'rect':
            dst2 = ((X**2) + (Y**2)) # pow2 of distances
            dst2 = (dst2, dst2)
        elif self.topology == 'hexa':
            Y1 = Y
            Y2 = Y.copy()
            Y1[1::2] -= 0.5
            Y2[1::2] += 0.5
            dst2 = ((X**2) + (Y1**2)), ((X**2) + (Y2**2))

        data_gen = randomize(data)

        for t in range(tmax):
            radius = fradius(t)
            iradius = int(radius)
            vec = next(data_gen)
            i, j = self.bmu(vec)
            min_i = max(0, i - iradius)
            max_i = min(self.h, i + iradius + 1)
            min_j = max(0, j - iradius)
            max_j = min(self.w, j + iradius + 1)
            d2 = dst2[i&1][r0-i+min_i:r0-i+max_i,r0-j+min_j:r0-j+max_j]
            gk = np.exp(-d2 / (2 * (radius * k) ** 2))
            net = self.net[min_i:max_i,min_j:max_j]     # get view
            net += falpha(t) * (vec - net) * gk[:,:,np.newaxis]

        if self.verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))


    def tag_classes(self, data, classes):
        self.a = defaultdict(Counter)
        self._inertie = np.zeros((self.h, self.w))
        self.classes = set(classes)
        if self.verbose:
            t1 = time()
            print('  tagging SOM network with {} classes ({} items)'.format(len(self.classes), len(data)))
        for item, cls in zip(data, classes):
            i, j = self.bmu(item)
            self.a[i,j][cls] += 1
            self._inertie[i,j] += ((item - self.net[i,j]) ** 2).sum() ** 0.5
        self.inertie = self._inertie.sum() / len(data)
        for cnt in self.a.values():
            if len(cnt) > 0:
                cnt._sum = sum(cnt.values())
                cnt._tag, cnt._max = max(cnt.items(), key=itemgetter(1))
        if self.verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))

    def umatrix(self):
        def eucdst(v1, v2):
            return sum((v1 - v2) ** 2) ** 0.5
        umat = np.zeros((self.h, self.w))
        for i,j in product(range(self.h), range(self.w)):
            idx = [(i+y, j+x) for (y,x) in [(1,0),(0,1),(-1,0),(0,-1),(-1,-1),(-1,1),(1,-1),(1,1)]]
            idx = [(y,x) for (y,x) in idx if 0 <= y < self.h and 0 <= x < self.w]
            ndst = np.array([eucdst(self.net[yx], self.net[i,j]) for yx in idx])
            umat[i,j] = ndst.mean()
        return umat

    def refine(self, data, classes, alpha=0.001):
        "implement LVQ-SOM"
        if self.verbose:
            t1 = time()
            print('  refine the network')
        assert hasattr(self, 'a'), 'network is must to be calibrated'
        X, Y = np.mgrid[-1.:2., -1.:2.]
        dst2 = ((X**2) + (Y**2)) # pow2 of distances
        for v, cls in zip(data, classes):
            i, j = self.bmu(v)
            guess = self.predict(v, 0.)
            if guess is not None:
                guess = guess[0]
            min_i = max(0, i - 1)
            max_i = min(self.h ,i + 1)
            min_j = max(0, j - 1)
            max_j = min(self.w, j + 1)
            #d2 = dst2[min_i:max_i,min_j:max_j]
            #d2 = dst2[1-i+min_i:1-i+max_i,1-j+min_j:1-j+max_j]
            #gk = np.exp(-d2 / 0.85)  # 0.2 ~ 2 * 0.4^2
            net = self.net[min_i:max_i,min_j:max_j]     # get view
            d = alpha * (v - net) # * gk[:,:,np.newaxis]
            if cls == guess:
                net += d
            else:
                net -= d
        if self.verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))

    def visualize(self):
        # pprint(self.a)
        for i in range(self.h):
            l = [max(self.a[i,j].items(), key=itemgetter(1))[0] if self.a[i,j] else '-' for j in range(self.w)]
            print(' '.join(str(n) for n in l))

    def _get_tags(self, vec):
        return self.a[self.bmu(vec)]

    def predict(self, vec, p=0.6):
        cnt = self._get_tags(vec)
        if not cnt:
            return None
        proba = cnt._max / cnt._sum
        if proba < p:
            return None
        return cnt._tag, proba

    def predict2(self, vec, p=0.5):
        i, j = self.bmu(vec)
        cnt = self.a[i, j]
        if not cnt or cnt._max / cnt._sum < p:
            neighbors = []
            for di, dj in [(-1,-1),(-1,1),(1,-1),(1,1),(-1,0),(1,0),(0,-1),(0,1)]:
                ii, jj = i + di, j + dj
                if 0 <= ii < self.h and 0 <= jj < self.w and self.a[ii,jj]:
                    neighbors.append((ii,jj))
            lst = sorted(neighbors, key=lambda idx: ((self.net[idx] - vec) ** 2).sum())
            for idx in lst:
                cnt = self.a[idx]
                if cnt and cnt._max / cnt._sum >= p:
                    return cnt._tag,
        else:
            return cnt._tag,

    def predict3(self, vec, p=0.65):
        i, j = self.bmu(vec)
        cnt = self.a[i, j]
        if not cnt or cnt._max / cnt._sum < p:
            distances = ((self.net - vec) ** 2).sum(axis=2)
            for ii, jj in zip(*np.unravel_index(np.argsort(distances, axis=None), distances.shape)):
                cnt = self.a[ii,jj]
                if cnt and cnt._max / cnt._sum >= p:
                    return cnt._tag, 
        else:
            return cnt._tag,

    def precision(self):
        arr = np.zeros((self.h, self.w))
        for i, j in product(range(self.h), range(self.w)):
            cnt = self.a[i,j]
            arr[i,j] = cnt._max / cnt._sum if cnt else 0
        return arr
