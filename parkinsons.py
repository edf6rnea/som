import csv
from random import shuffle
import numpy as np
from som import SOM
from utils import normalize_dimensions


##_keys = ['MDVP:Fo(Hz)','MDVP:Fhi(Hz)','MDVP:Flo(Hz)',
##         'MDVP:Jitter(%)','MDVP:Jitter(Abs)',
##         'MDVP:RAP','MDVP:PPQ',
##         'Jitter:DDP',
##         'MDVP:Shimmer','MDVP:Shimmer(dB)']

_keys = [
    #'MDVP:Fo(Hz)',
    #'MDVP:Fhi(Hz)',
    #'MDVP:Flo(Hz)',
    #'MDVP:Jitter(%)',
    #'MDVP:Shimmer',
    #'Jitter:DDP',
    #'MDVP:RAP',
    #'MDVP:PPQ',
    #'MDVP:APQ',
    #'Shimmer:APQ3',
    #'Shimmer:APQ5',
    #'Shimmer:DDA',
    #'RPDE',
    'DFA',
    'spread1',
    'spread2',
    #'D2',
    'PPE'
]


def selector(row, keys=_keys):
    return row['status'], np.array(list(map(float, (row[k] for k in keys))))

def load():
    with open('datasets/parkinsons.data') as fp:
        reader = csv.DictReader(fp)
        data = [selector(row) for row in reader]
        normalize_dimensions(data)
        return data


if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    SZ = 12

    som = SOM(SZ,SZ,K)
    som.train(data, 700, 0.95, SZ * 0.5)
    som.train(data, 5000, 0.05, 1.5)
    som.tag_classes(data, clss)

    som.visualize()
