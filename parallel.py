import numpy as np
from som2 import SOM

class ParallelSOM(SOM):
    def __init__(self, w, h, train_data, n):
        super().__init__(w, h)
        self.__data = train_data
        self._dim = w * h
        self._splitted = np.array_split(train_data, n)
        self._soms = [SOM(w, h, data) for data in self._splitted]

    def train(self, *args, **kwargs):
        for som in self._soms:
            som.train(*args, **kwargs)
        data = np.concatenate([som.codebook.reshape((self._dim, -1)) for som in self._soms])
        super().update_data(data)
        super().train(*args, **kwargs)
        #HACK
        super().update_data(self.__data)
        activation_map = super().get_surface_state()
        Y, X = np.unravel_index(activation_map.argmin(axis=1),
                                (self._n_rows, self._n_columns))
        self.bmus = np.vstack((X,Y)).T
