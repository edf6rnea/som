import numpy as np
from random import shuffle
from som import SOM
from utils import normalize_dimensions


def load():
    data = []
    with open('datasets/chainlink.vec', 'r') as fpv, open('datasets/chainlink.cls', 'r') as fpc:
        for lv, lc in zip(fpv, fpc):
            l = [float(x) for x in lv.split()[:-1]]
            v = np.array(l)
            c = lc.split()[-1]
            data.append([c, v])
    normalize_dimensions(data)
    return data


if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    TRAIN_LEN = 600
    SZ = 20

    som = SOM(SZ,SZ,K)
    som.train(data[:TRAIN_LEN], 1000, alpha0=0.95, radius0=SZ*0.5, k=0.3)
    som.train(data[:TRAIN_LEN], 5000, alpha0=0.05, radius0=1.5, k=0.3)
    som.tag_classes(data[:TRAIN_LEN], clss[:TRAIN_LEN])

    l = []
    n, k, u = 0, 0, 0
    for cls, vec in zip(clss[TRAIN_LEN:], data[TRAIN_LEN:]):
        guess = som.predict(vec)
        if guess != None:
            if guess[0] == cls:
                k += 1
            n += 1
            l.append(int(guess[0]) - int(cls))
        else:
            u += 1

    print('{} samples testeds'.format(n + u))    
    print('correct guesses: {:%}, {} unkowns'.format(k / n, u))

    a = np.array(l)
    print('error average = {}, std = {}'.format(np.fabs(a).mean(), a.std()))

    som.visualize()
