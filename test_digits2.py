import numpy as np
import pylab as pl
from mnist import load_mnist
from som2 import SOM
from parallel import ParallelSOM
from sklearn.metrics import classification_report
from time import time
from contextlib import contextmanager


ZOOM = 0.5
SHAPE = (12,12)
HEIGHT, WIDTH = 20, 30
EPOCHS = 10

@contextmanager
def timer(*args, **kw):
    print(*args, **kw)
    t1 = time()
    yield
    print(' ... elapsed %.3s' % (time()-t1))


# setup pylab
pl.rcParams.update(pl.rcParamsDefault)
pl.gray()


def plot_map(codebook, shape=SHAPE, filename=None):
    map_ = np.vstack([np.hstack([codebook[i,j].reshape(shape)
                                 for j in range(WIDTH)])
                      for i in range(HEIGHT)])
    pl.matshow(map_)
    if filename is not None:
        pl.savefig(filename)
    else:
        pl.show()

# KNN produce 96.51 %

N = 6

if __name__ == '__main__':

    with timer('load data'):
        train_data, train_target = load_mnist(dataset='training', zoom=ZOOM, crop=2)
        test_data, test_target = load_mnist(dataset="testing", zoom=ZOOM, crop=2)
        # normalize to unit space
        train_data = np.divide(train_data, 255, dtype=np.float32)
        test_data = np.divide(test_data, 255, dtype=np.float32)

    som = ParallelSOM(WIDTH, HEIGHT, train_data, n=N)
    som2 = SOM(WIDTH, HEIGHT, train_data)

    with timer('training Parallel SOM'):
        som.train(scale0=0.2, epochs=EPOCHS)

    with timer('training SOM'):
        som2.train(scale0=0.2, epochs=EPOCHS)

    # plot networks
    plot_map(som.codebook, filename='normal.png')
    plot_map(som.codebook, filename='parallel.png')
    #for i, net in enumerate(som._soms):
    #    plot_map(net.codebook, filename='parallel_net%d.png' % i)

    with timer('tagging parallel SOM'):
        som.tag_classes(train_target)

    with timer('tagging SOM'):
        som2.tag_classes(train_target)

    with timer('predict (parallel)'):
        X_labels = som.predict(test_data, unkown=-1)
        #X_labels = som.predict(train_data, unkown=-1)

    #print(classification_report(train_target, X_labels))
    print(classification_report(test_target, X_labels))

    with timer('predict (normal)'):
        X_labels = som2.predict(test_data, unkown=-1)
        #X_labels = som.predict(train_data, unkown=-1)

    #print(classification_report(train_target, X_labels))
    print(classification_report(test_target, X_labels))

    exit()

    splitted = np.array_split(train_target, N)
    for i, (net, target) in enumerate(zip(som._soms, splitted)):
        net.tag_classes(target)
        X = net.predict(test_data, unkown=-1)
        print(i, '\n', classification_report(test_target, X))
        #X = net.predict(net._data, unkown=-1)
        #print(i, '\n', classification_report(target, X))
