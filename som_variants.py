import numpy as np
from collections import defaultdict, Counter
from operator import itemgetter
from time import time
from som import SOM


class ParallelSOM(SOM):
    def __init__(self, w, h, dim, n):
        super().__init__(w, h, dim)
        self._n = n
        self._soms = [SOM(w, h, dim) for data in range(n)]

    def split_data(self, data):
        P = np.linspace(0, 1, self._n + 1)
        rnd = np.random.rand(len(data))
        spaces = [(P[i] <= rnd) & (rnd < P[i+1]) for i in range(self._n)]
        return [data[sp] for sp in spaces]

    def train(self, data, tmax, *args, **kwargs):
        data_partition = self.split_data(data)
        for som, partition in zip(self._soms, data_partition):
            som.train(partition, int(tmax / self._n), *args, **kwargs)
        new_data = np.concatenate([som.net.reshape((-1, self.dim)) for som in self._soms])
        k = int(tmax / len(data) * len(new_data))
        super().train(new_data, k, *args, **kwargs)


class DeepSOM:
    #TODO:
    # use more uniform filters (convolution?)
    # how to use it in classification ?
    def __init__(self, h, w, dim, n=8, verbose=True):
        self.n = n
        self._h, self._w = h, w
        self._dim = dim
        self._filters = self._make_filters(2)
        self._dims = [a.sum() for a in self._filters]
        self._div = np.vstack(self._filters).sum(axis=0)
        self.nets = [SOM(h, w, d, verbose=False) for d in self._dims]
        self._verbose = verbose

    def _make_filters2(self):
       div = 2. / self.n
       return [np.random.rand(self._dim) < div for _ in range(self.n)]

    def _make_filters(self, ncopy=1):
        a = np.arange(self._dim * ncopy) % self._dim
        np.random.shuffle(a)
        idx = np.array_split(a, self.n)
        lst = []
        for ix in idx:
            x = np.zeros(self._dim, dtype=bool)
            x[ix] = True
            lst.append(x)
        return lst

    def train(self, data, niter, *args, **kwargs):
        if self._verbose:
            t1 = time()
            print('  training DeepSOM %d nets of %dx%d units' %
                    (self.n, self._h, self._w))
            print('\tnb samples = %d, %d iterations' % (len(data), niter))
            print('\ta0 = {:.3}, r0 = {:.3}'.format(alpha0, radius0))
        for net, filt in zip(self.nets, self._filters):
            net.train(data[:,filt], niter, *args, **kwargs)
        if self._verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))

    def _vec(self, vec):
        v2 = np.zeros(vec.shape)
        for net, filt in zip(self.nets, self._filters):
            v2[filt] += net.net[net.bmu(vec[filt])]
        return v2 / self._div



class MultiSOM:
    def __init__(self, h, w, dim, n=8, verbose=True):
        self.n = n
        self.nets = [SOM(h, w, dim, verbose=True) for _ in range(n)]
        self.verbose = verbose

    def train(self, data, niter, *args, **kwargs):
        if self.verbose:
            t1 = time()
            print('  training %d SOM nets' % self.n)
        for som in self.nets:
            som.train(data, niter, *args, **kwargs)
        if self.verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))

    def tag_classes(self, data, classes):
        if self.verbose:
            t1 = time()
            print('  tagging SOM network')
        for som in self.nets:
            som.tag_classes(data, classes)
        if self.verbose:
            t2 = time()
            print('    elapsed {} s'.format(t2-t1))

    def _get_tags(self, vec):
        lst = [som._get_tags(vec) for som in self.nets]
        lst = [cnt for cnt in lst if cnt and cnt._max / cnt._sum > 0.85]
        return sum(lst, Counter())

    def predict(self, vec):
        cnt = self._get_tags(vec)
        if len(cnt) == 0:
            return None
        elt, nb = max(cnt.items(), key=itemgetter(1))
        proba = nb / sum(cnt.values())
        if proba < 0.5:
            return None
        return elt, proba
