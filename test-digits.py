import numpy as np
from collections import Counter
import pylab as pl
from mnist import load_mnist
from sklearn.metrics import classification_report
from som import SOM


ZOOM = 0.5 # 1 / 3
SHAPE = (12,12) # (8,8)

train_data, train_target = load_mnist(dataset='training', zoom=ZOOM, crop=2)
test_data, test_target = load_mnist(dataset="testing", zoom=ZOOM, crop=2)

#train_data = train_data / 255
#test_data = test_data / 255

train_data = np.divide(train_data, 255, dtype=np.float32)
test_data = np.divide(test_data, 255, dtype=np.float32)


N, K = train_data.shape; # print(train_data.shape)
TRAIN_LEN = N


#SZI, SZJ = 45, 60  # correct guesses: 94.76%, 0 unkowns
#SZI, SZJ = 30, 40
SZI, SZJ = 20, 30


def plot_map(codebook, shape, filename=None):
    mapping = np.vstack([np.hstack([codebook[i,j].reshape(shape)
                                 for j in range(SZJ)])
                      for i in range(SZI)])
    pl.matshow(mapping, cmap='gray')
    if filename is not None:
        pl.savefig(filename)
    else:
        pl.show()


def predict(estimator, data):
    X = [estimator.predict(v, 0.1) for v in data]
    return [t[0] if t is not None else -1 for t in X]


# KNN produce 96.51 %


if __name__ == '__main__':

    #som = MultiSOM(SZI, SZJ, K, 6)
    #som = DeepSOM(SZI, SZJ, K, n=6)
    som = SOM(SZI, SZJ, K)

    # center network to data
    #center = np.random.uniform(-0.1, 0.1, som.net.shape) + train_data.mean(axis=0)
    #som.net = center

    som.train(train_data, 5 * TRAIN_LEN,
              alpha0=0.7, alphaN=0.005, radiusN=0.01, k=1)
#              acooling='exponential', rcooling='linear', k=0.25)

#    plot_map(som.net, SHAPE)

    som.tag_classes(train_data, train_target)

    #som.visualize()

    print(classification_report(test_target, predict(som, test_data)))

    print(classification_report(train_target, predict(som, train_data)))
