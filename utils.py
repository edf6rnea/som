import numpy as np
from time import time
import contextlib


# some usefuls import 
from sklearn.preprocessing import normalize, scale
from sklearn.metrics import classification_report


@contextlib.contextmanager
def chrono(*args, **kw):
    print(*args, **kw)
    t1 = time()
    yield
    print(' ... elapsed %.3f' % (time() - t1))


def normalize_dimensions(data):
    """
    data is list of [(cls, features_vec), ...]
    """
    a = np.array([v for _, v in data])
    amin = np.min(a, axis=0)
    amax = np.max(a, axis=0)
    delta = amax - amin
    for _, v in data:
        v -= amin
        v /= delta
    # a -= amin
    # a /= delta
    # return a
