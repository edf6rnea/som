import somoclu
import numpy as np
from collections import defaultdict, Counter
from operator import itemgetter
from itertools import product


class SOM(somoclu.Somoclu):

    def tag_classes(self, labels):
        self._a = defaultdict(Counter)
        for i, cls in enumerate(labels):
            x, y = self.bmus[i]
            self._a[y,x][cls] += 1
        for cnt in self._a.values():
            if len(cnt) > 0:
                cnt._sum = sum(cnt.values())
                cnt._tag, cnt._max = max(cnt.items(), key=itemgetter(1))

    def predict(self, data, unkown=None):
        shape = self._n_rows, self._n_columns
        mat = self.get_surface_state(data)
        Y, X = np.unravel_index(mat.argmin(axis=1), shape)
        lst = []
        for i, j in zip(Y, X):
            cnt = self._a[i, j]
            #TODO: use proba ?
            if not cnt:
                lst.append(unkown)
            else:
                lst.append(cnt._tag)
        return np.array(lst)

    def predict2(self, data, unkown=None, p=0.65):
        assert 0 <= p <= 1, 'p must be in range [0..1]'
        shape = self._n_rows, self._n_columns
        mat = self.get_surface_state(data)
        Y, X = np.unravel_index(mat.argmin(axis=1), shape)
        lst = []
        for i, j, vec in zip(Y, X, mat):
            cnt = self._a[i, j]
            if not cnt or cnt._max / cnt._sum < p:
                neighbors = []
                for di, dj in [(-1,-1),(-1,1),(1,-1),(1,1),(-1,0),(1,0),(0,-1),(0,1)]:
                    ii, jj = i + di, j + dj
                    if 0 <= ii < self._n_rows and 0 <= jj < self._n_columns:
                        cnt = self._a[ii, jj]
                        if cnt and cnt._max / cnt._sum >= p:
                            neighbors.append((ii,jj))
                if neighbors:
                    idx = min(neighbors, key=lambda ij: vec[ij[0]*self._n_columns+ij[1]])
                    lst.append(self._a[idx]._tag)
                else:
                    lst.append(unkown)
            else:
                lst.append(cnt._tag)
        return np.array(lst)

    def precision(self):
        arr = np.zeros((self._n_rows, self._n_columns))
        for i, j in product(range(self._n_rows), range(self._n_columns)):
            cnt = self._a[i,j]
            arr[i,j] = cnt._max / cnt._sum if cnt else 0
        return arr
