import numpy as np
from random import shuffle, seed
from som import SOM

seed(42)

def load():
    data = []
    encountered = set()
    #with open('datasets/rgb.txt', 'r') as fp:
    # very differents results !
    with open('datasets/Resene-2010-rgb.txt', 'r') as fp:
        for line in fp:
            if line.startswith('!'): continue
            r, g, b, *color = line.strip().split()
            rgb = tuple(map(int, (r, g, b)))
            if rgb not in encountered:
                encountered.add(rgb)
                data.append(['-'.join(color), np.array(rgb) / 255])
    return data


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    K = len(data[0])

    SZ = 25

    som = SOM(SZ,SZ,K)
    som.train(data, 1000,
              alpha0=0.95, alphaN=0.0,
              radius0=SZ*0.5, radiusN=0.0, k=0.8)

    plt.imshow(som.net)
    plt.show()

    som.train(data, 5000,
              alpha0=0.01, alphaN=0.0,
              radius0=2.5, radiusN=0.0, k=0.6)
    som.tag_classes(data, clss)

    #som.visualize()
    plt.imshow(som.net)
    plt.show()

    print('values in range', som.net.min(axis=(0,1)), som.net.max(axis=(0,1)))
    #plt.imsave('som.png'.format(lap), som.net)
