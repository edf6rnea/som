import numpy as np
from random import shuffle
from som import SOM
from sklearn.preprocessing import minmax_scale


# this is similar to regression task, must be adapted for SOM
# (maybe by use classes centroids instead of instances majority)

def load():
    data = []
    with open('datasets/abalone.data.txt') as fp:
        sex = {'M':0,'F':1,'I':0.5}
        for line in fp:
            l = line.strip().split(',')
            if len(l) != 9: continue
            v = np.array([sex[l[0]]] + [float(e) for e in l[1:-1]])
            data.append([l[-1], v])
    return data


def predict(som, vec):
    cnt = som._get_tags(vec)
    if not cnt:
        return None
    lst = [int(cls) * n for cls, n in cnt.items()]
    return sum(lst) / sum(cnt.values())

if __name__ == '__main__':
    DATA = load()
    shuffle(DATA)
    
    clss, data = zip(*DATA)
    data = np.array(data)
    data = minmax_scale(data)
    K = len(data[0])

    TRAIN_LEN = 3000 # int(len(data) * 0.55)
    SZ = 10 # int(TRAIN_LEN ** 0.45)

    som = SOM(10, 15, K)
    som.train(data[:TRAIN_LEN], 10 * TRAIN_LEN, alpha0=0.5, alphaN=0.005, radiusN=0.1, k=0.35)
    #som.train(data[:TRAIN_LEN], 30 * TRAIN_LEN, alpha0=0.05, radius0=1.5, k=0.3)
    som.tag_classes(data[:TRAIN_LEN], clss[:TRAIN_LEN])

    #exit(0)

    l, n, k, u = [], 0, 0, 0
    for cls, vec in zip(clss[TRAIN_LEN:], data[TRAIN_LEN:]):
        #guess = som.predict(vec, 0.1)
        guess = predict(som, vec)
        if guess != None:
            #if guess[0] == cls:
            #    k += 1
            if round(guess) == int(cls):
                k += 1
            n += 1
            l.append(guess - int(cls))
            #l.append(int(guess[0]) - int(cls))
        else:
            u += 1

    print('{} samples testeds'.format(n + u))    
    print('correct guesses: {:%}, {} unkowns'.format(k / n, u))

    a = np.array(l)
    print('error average = {}, std = {}'.format(np.fabs(a).mean(), a.std()))

    if som.w < 25 and som.h < 40:
        som.visualize()

    import matplotlib.pyplot as plt

    plt.hist(a, bins=len(set(clss[TRAIN_LEN:])))
    plt.show()

    # best results without normalisation, but it's completely normal
